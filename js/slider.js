function setCarouselRightHeight() {
    var $owlContentItems = $('.owl-content-item'),
        topStart         = $('header nav').position().top,
        owlTopPos        = $('.owl-carousel-content').position().top,
        allHeight        = $('body').height(),
        footerHeight     = $('.footer-info').height(),
        footerWrapper    = $('.footer-wrapper').height(),
        bxPanelheight    = $('#bx-panel').height(),
        resultHeight     = allHeight-topStart-owlTopPos-bxPanelheight-footerHeight-footerWrapper;

    if (resultHeight < 300) {
        resultHeight = 300;
    };

    $owlContentItems.height(resultHeight);
    $('.owl-carousel-content .owl-stage-outer').height(resultHeight);
}

jQuery(document).ready(function ($) {

    var owl = $('.bg-carousel');
    var owlcontent = $('.owl-carousel-content');
    var owlDelay = 1200;

    if (owlcontent.length) {
        //$(window).on('load resize', function () {
        setCarouselRightHeight();
        //});


        $.each(owlcontent.find('.owl-content-item'), function(index, val) {
            owl.append('<div class="item" style="background-image: url(\'' + $(this).data('bgImage') + '\');"></div>');
        });

        var userAgent = navigator.userAgent;
        if (userAgent.indexOf('Version/5.1') == -1) {

            owl.owlCarousel({
                loop: true,
                animateOut: 'fadeOut',
                autoHeight: true,
                items: 1,
                touchDrag: false,
                mouseDrag: false,
                nav: false,
                onInitialize: function () {
                    $('body').css('overflow', 'hidden');
                },
                onInitialized: function () {
                    setTimeout(function () {
                        owl.removeClass('loading');
                    }, owlDelay);
                    $('body').css('overflow', 'auto');
                }
            });

            owlcontent
                .owlCarousel({
                    loop: true,
                    animateOut: 'fadeOut',
                    autoHeight: true,
                    items: 1,
                    nav: false,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    //autoplayHoverPause: true,
                    onInitialized: function () {
                        setTimeout(function () {
                            owlcontent.removeClass('invisible');
                        }, owlDelay);
                    }
                })
                .on('translate.owl.carousel', function (event) {
                    owl.trigger('to.owl.carousel', event.item.index - 2)
                });

        }else{

            $('body').find('.bg-carousel').removeClass('loading');
            $('body').find('.owl-carousel-content').removeClass('invisible');

            owl.owlCarousel({
                loop: true,
                animateOut: 'fadeOut',
                autoHeight: true,
                items: 1,
                touchDrag: false,
                mouseDrag: false,
                nav: false,
            });

            owlcontent
                .owlCarousel({
                    loop: true,
                    animateOut: 'fadeOut',
                    autoHeight: true,
                    items: 1,
                    nav: false,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplayHoverPause: true,
                })
                .on('translate.owl.carousel', function (event) {
                    owl.trigger('to.owl.carousel', event.item.index - 2)
                });


        }
    };
});