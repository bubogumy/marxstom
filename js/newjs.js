$(document).ready(function () {
    showAndHideSelect();
    sendReview("#form-add-review");
    sendReview("#form-add-review-main");
    sendMail("#js-patient_form", "/priem-form/", "#priem", "#success-priem");
    sendMail("#js-add-vacancy-form", "/vacancy-form/", null, "#success-vacancy");
    rateFilter();
    hoverImage();

    $('#wait_comp').hide();
    function showAndHideSelect() {
        var select = $('#person-radio');
        select.on('click', function () {
            $('#personal-select').css({
                'display':'inline-block'
            });
        });
        $('#clinic').on('click', function () {
            $('#personal-select').css({
                'display':'none'
            });
        })
    }

    function sendMail(item, url, modalId, ModalSuccessId) {
        $(item).on('submit', function (e) {
            // Добавление решётки к имени ID
            var formNm = $(this);
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                success: function (data) {
                    // Вывод текста результата отправки
                    if(modalId) {
                        $(modalId).modal('hide');
                    }
                    $(ModalSuccessId).modal('show');
                },
                error: function (jqXHR, text, error) {
                    // Вывод текста ошибки отправки
                    $(formNm).html(error);
                }
            });
            return false;
        });
    }

    function sendReview(item) {
        $(item).on('submit', function (e) {
            // Добавление решётки к имени ID
            var formNm = $(this);
            var selectValue = $('select[name="personal"]');
            var urlValue = selectValue.val();
            var urlTo = '/personal/'+urlValue+'/';
            $.ajax({
                type: "POST",
                url: urlTo,
                data: formNm.serialize(),
                success: function (data) {
                    // Вывод текста результата отправки
                    $('#review').modal('hide');
                    $('#success-review').modal('show');
                },
                error: function (jqXHR, text, error) {
                    // Вывод текста ошибки отправки
                    $(formNm).html(error);
                }
            });
            return false;
        });
    }

    function rateFilter() {
        $('#rate_filter').on('change', function () {
            var rateValue = $('input[name="rate_filter"]:checked').val();
            $('#wait_comp').show();
            $.ajax({
                type: "POST",
                url: '/reviews/',
                data: $(this).serialize(),
                success: function (data) {

                    // Вывод текста результата отправки
                    $('#msnry-items').empty();
                    var content = $(data).find('#msnry-items').html();
                    $('#msnry-items').append(content);
                    masonryReload();
                    $('#wait_comp').hide();

                },
                error: function (jqXHR, text, error) {
                    // Вывод текста ошибки отправки
                    $(formNm).html(error);
                }
            });
            return false;
        })
    }


    function masonryReload() {
        var msnryBox = $('.box');
        if (msnryBox.length > 1) {
            var msnryContainer = document.querySelector('#msnry-items');
            msnry = new Masonry(msnryContainer, {
                itemSelector: '.box',
                //stamp: ".stamp"
            });
        };
    }

    function hoverImage() {
        $('.js-hover-works').on('mouseover', function () {
            var hoverImage = $(this).data('hover');
            $(this).attr('src', hoverImage);
        })
        $('.js-hover-works').on('mouseleave', function () {
            var hoverImage = $(this).data('src');
            $(this).attr('src', hoverImage);
        })
    }
});