$(document)
	// ���� �������� ����� � ���������
	.on('submit', '[data-ajax-submit]', function() {
		var $this = $(this),
			laddaLoad,
			options = {
				beforeSubmit: processStart,
				success: processDone,
				error: processFail
			};

		$this.ajaxSubmit(options);

		return false;
	})
	.on('submit', '[data-ladda-submit]', function(e) {
		e.preventDefault();

		var $this = $(this),
			progress = 0,
			laddaLoad = $this.find('.ladda-button').ladda();
		laddaLoad.ladda('start');

		var interval = setInterval(function () {
			progress = Math.min(progress + Math.random() * 0.2, 1);
			laddaLoad.ladda('setProgress', progress);

			if (progress === 1) {
				laddaLoad.ladda('stop');
				clearInterval(interval);
				$this.removeAttr('data-ladda-submit');
				$this.find('.ladda-button').trigger('click');
			}
		}, 100);
	})
	.on('click', '[data-fake-form] input, [data-fake-form] textarea', function() {
		var $this = $(this),
			$fakeForm = $this.closest('[data-fake-form]'),
			formProp = $fakeForm.data('fakeForm');

		//console.log('fakeForm: ', attributesToString($fakeForm.get(0).attributes, true));
		var bWasBitrixAjax = false;
		if (formProp == '') {
			formProp = attributesToString($fakeForm.get(0).attributes, true);
			bWasBitrixAjax = true;
		}

		$fakeForm.removeAttr('data-fake-form').wrap('<form ' + formProp + ' />');
		$this.focus();
		if (bWasBitrixAjax) {
			//console.log(this.form);
			var form = this.form;
			var ajaxId = form.elements['bxajaxid'].value;
			if (top.BX) {
				top.BX.bind(form, 'submit', function() {
					BX.ajax.submitComponentForm(this, 'comp_'+ajaxId, true);
				});
			}
		}
	});
function attributesToString(attr, bExcludeId) {
	bExcludeId = (typeof(bExcludeId) == 'undefined') ? false : true;
	var str = '';
	for(var k=0,l=attr.length;k<l;k++) {
		var itm = attr.item(k);
		//console.log('itm: ', itm);
		if (bExcludeId && (itm.nodeName == 'id' || itm.nodeName == 'data-fake-form')) {
			continue;
		}
		str += ' '+itm.nodeName+'="'+itm.nodeValue+'"';
	}
	return str;
}
// pre-submit callback
function processStart(formData, jqForm, options) {
	laddaLoad = jqForm.find('.ladda-button').ladda();
	laddaLoad.ladda('start');

	return true;
}

// post-submit callback
function processDone(responseText, statusText, xhr, $form) {
	//console.log('form: ', $form);
	var $responseText = $(responseText),
		responseResult = ($responseText.is('form')) ? $responseText.html() : responseText;

	var progress = 0;
	var interval = setInterval(function () {
		progress = Math.min(progress + Math.random() * 0.2, 1);
		laddaLoad.ladda('setProgress', progress);

		if (progress === 1) {
			laddaLoad.ladda('stop');
			clearInterval(interval);

			// ��� ���-�� ������ � ���������� �������
			if (statusText == 'success') {
				$form.html(responseResult);
				var $styler = $form.find('.styler');
				if ($styler.length) {
					$styler.selectize();
				};
			};
		}
	}, 100);
}

function processFail(responseText, statusText, xhr, $form) {
	alert('Error');
}