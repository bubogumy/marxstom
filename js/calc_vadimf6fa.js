// ���������� ����������.
var doc = $(document);

doc
	// ����������� ��������������	
		.on('click', '#toothCalcNextStepV', function () {
			var step = $('#toothCalcStep').val();
			step++;
			ToothCalcStepV(step, 'next');
		})
		.on('click', '#toothCalcPrevStepV', function () {
			var step = $('#toothCalcStep').val();
			step--;
			ToothCalcStepV(step, 'prev');
		})
	// ����������� ��������������
	;

// ����������� ��������������

	function ToothCalcStepV(step, button) {
		var $toothNew       = $('g.tooth-new'),
			$toothActive    = $('g.active'),
			$legendItem     = $('.legend-item'),
			$legendMissed   = $('#legend-missed'),
			$legendCrown    = $('#legend-crown'),
			$legendImplant  = $('#legend-implant'),
			$legendInBridge = $('#legend-in-bridge'),
			$legendBridge   = $('#legend-bridge');

		$legendItem.hide();

		var noPriceMsg = '� ������ ����� ���������� �� ����� ����������';

		switch (step) {
		case 1:
			if (button == 'prev') {
				buttonOff = false;

				$.each($toothActive, function () {
					this.removeClass('active');
				});

				if (missingTeeth.length > 0) {
					$.each($toothNew, function () {
						if ($.inArray($(this).data('toothId'), missingTeeth) > -1) {
							this.addClass('active');
							this.removeClass('missing');
						}
					});
				}
			}
			$('#toothCalcPrevStepV').hide(100);
			changeStepInfo(step);

			$legendItem.hide();

			break;
		case 2:
			$('#toothCalcNextStepV').show(100);
			if (button == 'next') {
				missingTeeth = [];
				$.each($toothActive, function () {
					missingTeeth.push($(this).data('toothId'));
					$(this).attr('class', 'tooth-new missing');
				});

			}
			if (button == 'prev') {
				buttonOff = false;
				$.each($toothNew.not('.inactive'), function () {
					$(this).attr('class', 'tooth-new');
				});
				if (missingTeeth.length > 0) {
					$.each($toothNew, function () {
						if ($.inArray($(this).data('toothId'), missingTeeth) > -1) {
							this.addClass('missing');
						}
					});
				}
				if (affectedTeeth.length > 0) {
					$.each($toothNew, function () {
						if ($.inArray($(this).data('toothId'), affectedTeeth) > -1) {
							$(this).attr('class', 'tooth-new active');
						}
					});
				}
			}
			
			$legendMissed.show();					

			$('#toothCalcPrevStepV').show(100);
			changeStepInfo(step);
			break;
		case 3:
			if (button == 'next') {
				buttonOff = true;
				affectedTeeth = [];
				$.each($('g.active'), function () {
					affectedTeeth.push($(this).data('toothId'));
					$(this).attr('class', 'tooth-new affected');
				});
				changeStepInfo(step);
				$('#toothCalcNextStepV').hide();
				var vars = 1;
				var str = '';
				if (missingTeeth.length > 0) {
					$.post(
						'/local/codenails/includes/ajax/teeth_calc_assemble.php', {
							missingTeeth: missingTeeth
						},
						function (data) {
							var price = 0;
							var bCurrency = true;

							result = JSON.parse(data);
							console.log(result);                           
							var bridgesCount = 0;
							var affectedCount = (affectedTeeth.length == 0) ? 0 : affectedTeeth.length;
							var affectedLength = affectedCount;
							if (result.BRIDGES.length > 0) {
								var implantantsInBridge = 0;
								for (var index in result.TEETH) {
									if (result.TEETH[index] == 'IMPLANT-IN-BRIDGE') {
										implantantsInBridge++;
									}
								}
								for (var index in result.BRIDGES) {
									if (index / index == 1 || index == 0) {
										bridgesCount += result.BRIDGES[index].length;
									}

									for (var indexBridges in result.BRIDGES[index]) {
										for (var indexAffected in affectedTeeth) {
											if (affectedTeeth[indexAffected] == result.BRIDGES[index][indexBridges]) {
												affectedLength--;
											}
										}
									}
								}
								
								str += '<p class="text-green"><input class="radio choice-radio" type="radio" name="choice" id="choice' + vars + '" data-bridges="' + JSON.stringify(result.BRIDGES) + '" data-bridges-count="' + (bridgesCount - implantantsInBridge) + '" data-spoons="' + result.SPOON_WITH_BRIDGES + '" data-implantants-count="' + result.IMPLANTS.length + '" data-crowns-count="' + affectedLength + '">';
								str += '<label for="choice' + vars + '"><span></span>';
								str += '��������� ' + result.BRIDGES.length + cnDeclination(result.BRIDGES.length, ' ���������|���|��|��') + cnDeclination(result.BRIDGES.length, ' ������|�|��|��') + ': ';
								for (var index in result.BRIDGES) {
									if (index/index == 1 || index == 0) {
										str +='<br>- �� ' + result.BRIDGES[index].length + cnDeclination(result.BRIDGES[index].length, ' �����|��|�|�') + '.';
									} 
								}

								if (result.IMPLANTS.length > 0) {
									str += '<br>��������� ' + result.IMPLANTS.length + cnDeclination(result.IMPLANTS.length, ' �������|�|��|��') + ' � �������������� ������������. ';
								}

								vars++;

								if (affectedLength > 0) {
									str += '<br>����������� ����������� ���������� ����� � ' + affectedLength + cnDeclination(affectedLength, ' ���|�|��|��') + '. ';
								}
								
								price = getCalculatedPriceV(JSON.stringify(result.BRIDGES), affectedTeeth, missingTeeth, bridgesCount - implantantsInBridge, result.IMPLANTS.length, affectedLength, result.SPOON_WITH_BRIDGES);
								if (price == '') {
									price = noPriceMsg;
									bCurrency = false;
								}

								str += '<i class="d-b mt10 text-muted">����: <b class="fz20 text-green"><i id="totalPrice">' + price + '</i>'+(bCurrency ? ' ���.' : '')+'</b><br></i>';
								str += '</label></p>';
							}

							str += '<p class="text-green"><input class="radio choice-radio" type="radio" name="choice" id="choice' + vars + '" data-bridges-count="0" data-spoons="' + result.SPOON_WITHOUT_BRIDGES + '" data-implantants-count="' + missingTeeth.length + '" data-crowns-count="' + affectedLength + '">';
							str += '<label for="choice' + vars + '"><span></span>';
							str += /*vars + ')*/ '��������� ' + missingTeeth.length + cnDeclination(missingTeeth.length, ' �������|�|��|��') + ' � �������������� ������������. ';
							if (affectedTeeth.length > 0) {
								str += '����������� ����������� ���������� ����� � ' + affectedTeeth.length + cnDeclination(affectedTeeth.length, ' ���|�|��|��') + '.';
							}

							price = getCalculatedPriceV(JSON.stringify(result.BRIDGES), affectedTeeth, missingTeeth, 0, missingTeeth.length, affectedLength, result.SPOON_WITHOUT_BRIDGES);

							if (price == '') {
								price = noPriceMsg;
								bCurrency = false;
							}
							else {
								bCurrency = true;
							}

							str += '<i class="d-b mt10 text-muted">����: <b class="fz20 text-green"><i id="totalPrice">' + price + '</i>'+(bCurrency ? ' ���.' : '')+'</b><br></i>';
							str += '</label></p>';
							str += '<i>* ������ �������� �������������� ����� �������� � �������</i><br>';
							if (result.TIP == 'true') {
								str += '<i>* �������� ��������� �������������� ����������.</i><br>';
							}
							str += "<i>* ��������� ������� � ������ ������������� ������������������� ����������� � ������������ SEMADOS</i><br><a class='btn btn-link btn-all-our-work mt20' href='/our_works/'>���������� ���� ������</a>";
							$('#toothCalcStepDescription').html(str);
						}
					);
				}
				else {
					if (affectedTeeth.length > 0) {
						str = '<p class="text-green"><input class="radio choice-radio" type="radio" name="choice" id="choice' + vars + '" data-bridges-count="0" data-implantants-count="0" data-crowns-count="' + affectedTeeth.length + '">';
						str += '<label for="choice' + vars + '"><span></span>';
						str += '����������� ����������� ���������� ����� � ' + affectedTeeth.length + cnDeclination(affectedTeeth.length, ' ���|�|��|��') + '.<br>';

						price = getCalculatedPriceV([], affectedTeeth, missingTeeth, 0, 0, affectedTeeth.length, 0);
						if (price == '') {
							price = noPriceMsg;
							bCurrency = false;
						}
						else {
							bCurrency = true;
						}

						str += '<div class="mt10">����: <br><b class="fz20 text-green"><span id="totalPrice">' + price + '</span>'+(bCurrency ? ' ���.' : '')+'</b><br></div>';
						str += '</label></p>';
					}
					else {
						str = '��������� ������� �� �������.<br>';
						$('#toothCalcNextStep').hide(100);
					}
					$('#toothCalcStepDescription').html(str);
				}
			}
			else {
				// ��� ����� ������� �� ���� ����� �������
				$('#toothCalcNextStepV').show(100);
				changeStepInfo(step);
				var vars = 1;
				var str = '';
				if (missingTeeth.length > 0) {
					$.post(
						'/local/codenails/includes/ajax/teeth_calc_assemble.php', {
							missingTeeth: missingTeeth
						},
						function (data) {
							result = JSON.parse(data);
							var bridgesCount = 0;
							var affectedCount = (affectedTeeth.length == 0) ? 0 : affectedTeeth.length;
							var affectedLength = affectedCount;
							if (result.BRIDGES.length > 0) {
								var implantantsInBridge = 0;
								for (var index in result.TEETH) {
									if (result.TEETH[index] == 'IMPLANT-IN-BRIDGE') {
										implantantsInBridge++;
									}
								}
								for (var index in result.BRIDGES) {
									if (index / index == 1 || index == 0) {
										bridgesCount += result.BRIDGES[index].length;
										for (var indexBridges in result.BRIDGES[index]) {
											for (var indexAffected in affectedTeeth) {
												if (affectedTeeth[indexAffected] == result.BRIDGES[index][indexBridges]) {
													affectedLength--;
												}
											}
										}
									}
								}
								str += '<p class="text-green"><input class="radio choice-radio" type="radio" name="choice" id="choice' + vars + '" data-bridges="' + JSON.stringify(result.BRIDGES) + '" data-bridges-count="' + (bridgesCount - implantantsInBridge) + '" data-implantants-count="' + result.IMPLANTS.length + '" data-crowns-count="' + affectedLength + '">';

								str += '<label for="choice' + vars + '"><span></span>';
								str += '��������� ' + result.BRIDGES.length + cnDeclination(result.BRIDGES.length, ' ���������|���|��|��') + cnDeclination(result.BRIDGES.length, ' ������|�|��|��') + ': ';
								for (var index in result.BRIDGES) {
									if (index/index == 1 || index == 0) {
										str +='<br>- �� ' + result.BRIDGES[index].length + cnDeclination(result.BRIDGES[index].length, ' ���||�|��') + '.';
									} 
								}

								if (result.IMPLANTS.length > 0) {
									str += '<br>��������� ' + result.IMPLANTS.length + cnDeclination(result.IMPLANTS.length, ' �������|�|��|��') + ' � �������������� ������������. ';
								}
								vars++;

								if (affectedLength > 0) {
									str += '<br>����������� ����������� ���������� ����� � ' + affectedLength + cnDeclination(affectedLength, ' ���|�|��|��') + '.';
								}
								str += '</label></p>';
							}
							str += '<p class="text-green"><input class="radio choice-radio" type="radio" name="choice" id="choice' + vars + '" data-bridges-count="0" data-implantants-count="' + missingTeeth.length + '" data-crowns-count="' + affectedCount + '">';
							str += '<label for="choice' + vars + '"><span></span>';
							str += /*vars + ')*/ '��������� ' + missingTeeth.length + ' ����������� � �������������� ������������ <br>';
							if (affectedTeeth.length > 0) {
								str += '����������� ����������� ���������� ����� � ' + affectedTeeth.length + cnDeclination(affectedTeeth.length, ' ���|�|��|��') + '.<br>';
							}
							str += '</label></p>';
							str += '<i>* ������ �������� �������������� ����� �������� � �������</i><br>';
							if (result.TIP == 'true') {
								str += '<i>* �������� ��������� �������������� ����������.</i><br>';
							}							
                            str += "<i>* ��������� ������� � ������ ������������� ������������������� ����������� � ������������ ALPHA-BIO</i><br><a class='btn btn-link btn-all-our-work mt20' href='/our_works/'><br>";
							$('#toothCalcStepDescription').html(str);
							$('#' + choiceId).attr('checked', true);
						}
					);
				}
				else {
					if (affectedTeeth.length > 0) {
						str = '<p class="text-green"><input class="radio choice-radio" type="radio" name="choice" id="choice' + vars + '" data-bridges-count="0" data-implantants-count="0" data-crowns-count="' + affectedTeeth.length + '">';
						str += '<label for="choice' + vars + '"><span></span>';
						str += '����������� ����������� ���������� ����� � ' + affectedTeeth.length + cnDeclination(affectedTeeth.length, ' ���|�|��|��') + '.<br>';
						str += '</label></p>';
					}
					else {
						str = '��������� ������� �� �������.<br>';
						$('#toothCalcNextStepV').hide(100);
					}
					$('#toothCalcStepDescription').html(str);
					$('#' + choiceId).attr('checked', true);
				}
			}
			$legendMissed.show();
			$legendCrown.show();
			$legendImplant.show();
			$legendInBridge.show();
			$legendBridge.show();

			break;
		case 4:
			// ��� ����� ����� ����� �������
			if (button == 'next') {
				var choice = $('input[name="choice"]:radio:checked');
				choiceId = choice.attr('id');
				if (!choice.attr('id')) {
					alert('���������� ������� ������� �������');
				}
				else {
					changeStepInfo(step);
					$('#toothCalcNextStepV').hide(100);
					var bridges = (choice.data('bridges')) ? choice.data('bridges') : false ;
				
					var bridgesCount = choice.attr('data-bridges-count');
					var implantantsCount = choice.attr('data-implantants-count');
					var crownsCount = choice.attr('data-crowns-count');
	
					var str = '';

					$.post(
						'/local/codenails/includes/ajax/teeth_prices_and_logs.php', {
							bridges: bridges,
							affectedTeeth: affectedTeeth,
							missingTeeth: missingTeeth,
							bridgesCount: bridgesCount,
							implantantsCount: implantantsCount,
							crownsCount: crownsCount,
						},
						function (data) {
							var result = JSON.parse(data);
							var bridgesPrice = bridgesCount * result.CROWNS[0];
							var implantantsPrice = implantantsCount * result.IMPLANTS[0];
							var crownsPrice = crownsCount * result.CROWNS[1];
							var totalPrice = bridgesPrice + implantantsPrice + crownsPrice;

							str += '<div class="mt20">����� ����: <br><b class="fz30 text-green"><span id="totalPrice">' + totalPrice + '</span> ���.</b><br></div>';
							str += "<br><i>* ��������� ������� � ������ ������������� ������������������� ����������� � ������������ ALPHA-BIO</i><br><a class='btn btn-link btn-all-our-work mt20' href='/our_works/'>���������� ���� ������</a>";
							$('#toothCalcStepDescription').html(str);
						}
					);
				}

			}
			$legendCrown.show();
			$legendImplant.show();
			$legendInBridge.show();
			$legendBridge.show();
			break;
		}
	}

	function getCalculatedPriceV (bridges, affectedTeeth, missingTeeth, bridgesCount, implantantsCount, crownsCount, spoonsCount) {

		var totalPrice = '-';

		$.ajax({
			url: '/local/codenails/includes/ajax/teeth_prices_and_logs_vadim.php',
			type: 'POST',
			async: false, // ��� ����� ���������� ������, ����� ���� �� �������
			data: {
				bridges: bridges,
				affectedTeeth: affectedTeeth,
				missingTeeth: missingTeeth,
				bridgesCount: bridgesCount,
				implantantsCount: implantantsCount,
				crownsCount: crownsCount,
                spoonsCount: spoonsCount,
			},
		})
		.done(function(data) {
				var result = JSON.parse(data);
				var bridgesPrice = bridgesCount * result.CROWNS[0];
				var implantantsPrice = implantantsCount * result.IMPLANTS[0];
				var crownsPrice = crownsCount * result.CROWNS[1];
                var spoonsPrice = spoonsCount * result.SPOONS[0];
				console.log(data);
				totalPrice = bridgesPrice + implantantsPrice + crownsPrice + spoonsPrice;
				if (result.ERROR > 0) {
					totalPrice = '';
				}
		})
		.fail(function() {
			console.error("error");
		});
		return totalPrice;
	}
// ����������� ��������������