<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="informationsystem/informationsystem_item" />
	</xsl:template>
	
	<xsl:template match="informationsystem_item">
		<div class="content news-item mb20" >
			<div class="col col-mb-12">
				<div class="news-item-date"><xsl:value-of select="date"/></div>
				<h3 class="h4 mt0"><xsl:value-of select="name"/></h3>
				<div class="clearfix"></div>

				<xsl:value-of select="text"/>
			</div>
		</div>
	</xsl:template>
	
</xsl:stylesheet>