<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="/informationsystem/informationsystem_item" />
	</xsl:template>
	
	<xsl:template match="informationsystem_item">
		<div class="item owl-content-item p-r" data-bg-image="{dir}{image_large}">
			<div class="h2 p-a" style="left: 0; bottom: 15px;">
			</div>
			<div class="p-a" style="top: 0; left: 0;">
				<div class="h1"><xsl:value-of select="name"/></div>
				<div class="h3"><xsl:value-of select="description" disable-output-escaping="yes"/></div>
				<p class="title_p">
					<xsl:value-of select="text" disable-output-escaping="yes"/>
				</p>
				<div class="h2 p-a">
					<xsl:if test="property_value[tag_name='link_slider']/value !=''">
						<a href="{property_value[tag_name='link_slider']/value}">Подробнее<br/></a>
					</xsl:if>
				</div>
			</div>
		</div>
	</xsl:template>
		
	</xsl:stylesheet>