<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

	<!-- Хлебные крошки -->

	<xsl:template match="/site">
		<ul class="bx-breadcrumb clearfix">
			<xsl:if test="count(*[@id]) &gt; 0">
				<li class="bx-breadcrumb-item" id="bx_breadcrumb_0" itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
					<a href="/" title="Главная" itemprop="url">Главная</a>
				</li>
				<xsl:apply-templates select="*[@id]" />
			</xsl:if>
		</ul>
	</xsl:template>

	<xsl:template match="*">

		<!-- Определяем адрес ссылки -->
		<xsl:variable name="link">
			<xsl:choose>
				<!-- Если внутренняя ссылка -->
				<xsl:when test="link != ''">
					<li class="bx-breadcrumb-item">
						<span>
							<xsl:value-of disable-output-escaping="yes" select="link"/>
						</span>
					</li>
				</xsl:when>
				<!-- Если внешняя ссылка -->
				<xsl:otherwise>
					<li class="bx-breadcrumb-item">
						<span>
							<xsl:value-of disable-output-escaping="yes" select="url"/>
						</span>
					</li>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<!-- Показывать ссылку? -->
		<xsl:choose>
			<xsl:when test="(show = 1 or active/node() and active = 1) and count(*[@id][link/node() or url/node()]) &gt; 0">
				<li class="bx-breadcrumb-item">
					<a href="{$link}">
						<xsl:value-of select="name"/>
					</a>
				</li>
			</xsl:when>
			<xsl:otherwise>
				<li class="bx-breadcrumb-item">
					<a href="{$link}">
						<xsl:value-of select="name"/>
					</a>
				</li>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:apply-templates select="*[@id][link/node() or url/node()]" />

	</xsl:template>
</xsl:stylesheet>