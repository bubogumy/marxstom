<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

	<xsl:template match="/">
		<xsl:apply-templates select="informationsystem" />
	</xsl:template>

	<xsl:template match="informationsystem">
		<div id="msnry-items" class="row">
			<xsl:apply-templates select="informationsystem_item" />
		</div>
	</xsl:template>

	<xsl:template match="informationsystem_item">
		<xsl:if test="@id != 167">
			<xsl:apply-templates select="comment" mode="personal"/>
		</xsl:if>
		<xsl:if test="@id = 167">
			<xsl:apply-templates select="comment" mode="clinic"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="comment" mode="personal">
		<div class="box col col-ld-4 col-6" id="bx_3218110189_16496">
			<div class="testimonial-item">
				<div class="testimonial-item-header">
					<h3 class="h4 p-r">
						<xsl:choose>
							<xsl:when test="grade >= 4">
								<span class="label-testimonials pluse">Положительный отзыв</span>
								<a class="testimonial-target pluse-target" href="{../url}" target="_self">
									о сотруднике: <xsl:value-of select="../name"/></a>
							</xsl:when>
							<xsl:when test="grade >= 3">
								<span class="label-testimonials neutral">Нейтральный отзыв</span>
								<a class="testimonial-target neutral-target" href="{../url}" target="_self">
									о сотруднике: <xsl:value-of select="../name"/></a>
							</xsl:when>
							<xsl:otherwise>
								<span class="label-testimonials minus">Отрицательный отзыв</span>
								<a class="testimonial-target minus-target" href="{../url}" target="_self">
									о сотруднике: <xsl:value-of select="../name"/></a>
							</xsl:otherwise>
						</xsl:choose>


						<div class="social-likes social-likes_single social-likes_vertical social-likes_visible social-likes_ready share" data-url="{url}" data-title="Отзыв о докторе - {name}" data-single-title="Поделиться">
							<span class="facebook" title="Facebook">Facebook</span>
							<span class="vkontakte" title="Вконтакте">Вконтакте</span>
						</div>
					</h3>
				</div>
				<div class="testimonial-item-text">
					<xsl:value-of select="text" disable-output-escaping="yes"/>
				</div>
				<div class="testimonial-footer">
					<span class="testimonial-footer-date">07.09.2018</span>
					<span class="testimonial-footer-like">
						<span class="like-item like js-like" data-vote-id="{@id}" data-vote="+"><span class="like-count">5</span></span>

						<span class="like-item dislike js-dislike" data-vote-id="{@id}" data-vote="-"><span class="like-count">0</span></span>
					</span>
				</div>
			</div>
		</div>
	</xsl:template>

	<xsl:template match="comment" mode="clinic">
		<div class="box col col-ld-4 col-6" id="bx_3218110189_16496">
			<div class="testimonial-item">
				<div class="testimonial-item-header">
					<h3 class="h4 p-r">
						<xsl:choose>
							<xsl:when test="grade >= 4">
								<span class="label-testimonials pluse">Положительный отзыв</span>
								<div class="testimonial-target pluse-target" >
									О клинике</div>
							</xsl:when>
							<xsl:when test="grade >= 3">
								<span class="label-testimonials neutral">Нейтральный отзыв</span>
								<div class="testimonial-target neutral-target" >
									О клинике</div>
							</xsl:when>
							<xsl:otherwise>
								<span class="label-testimonials minus">Отрицательный отзыв</span>
								<div class="testimonial-target minus-target" >
									О клинике</div>
							</xsl:otherwise>
						</xsl:choose>


						<div class="social-likes social-likes_single social-likes_vertical social-likes_visible social-likes_ready share" data-url="{url}" data-title="Отзыв о докторе - {name}" data-single-title="Поделиться">
							<span class="facebook" title="Facebook">Facebook</span>
							<span class="vkontakte" title="Вконтакте">Вконтакте</span>
						</div>
					</h3>
				</div>
				<div class="testimonial-item-text">
					<xsl:value-of select="text" disable-output-escaping="yes"/>
				</div>
				<div class="testimonial-footer">
					<span class="testimonial-footer-date">07.09.2018</span>
					<span class="testimonial-footer-like">
						<span class="like-item like js-like" data-vote-id="{@id}" data-vote="+"><span class="like-count">0</span></span>

						<span class="like-item dislike js-dislike" data-vote-id="{@id}" data-vote="-"><span class="like-count">0</span></span>
					</span>
				</div>
			</div>
		</div>
	</xsl:template>
	
</xsl:stylesheet>