<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="informationsystem" />
	</xsl:template>

	<xsl:template match="informationsystem">
		<xsl:apply-templates select="informationsystem_item" />
	</xsl:template>

	<xsl:template match="informationsystem_item">
		<div class="box col col-mb-6 col-4" id="{@id}">
			<div class="our-works-item p-r">
				<div class="our-works-item-image">
					<img class="desktop js-hover-works" data-hover="{dir}{image_small}" data-src="{dir}{image_large}" src="{dir}{image_large}" alt="{name}" title="{name}"/>
				</div> <!-- .our-works-item-image -->

				<div class="our-works-item-text clearfix">
					<div class="h4">
						<xsl:if test="property_value[tag_name='categoryName']/value !=''">
						Ортопедия:
						</xsl:if><span class="text-red"><xsl:value-of select="name"/></span>
					</div>

					Врач: <a href="{property_value[tag_name='doctor_url']/value}"><xsl:value-of select="property_value[tag_name='doctor']/value"/></a>
				</div>
			</div> <!-- .our-works-item -->
		</div>
	</xsl:template>
	
</xsl:stylesheet>