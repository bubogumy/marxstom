<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<xsl:template match="/">
		<xsl:apply-templates select="informationsystem/informationsystem_item" />
	</xsl:template>
	
	<xsl:template match="informationsystem_item">
		<div class="content news-item" id="{@id}">
			<div class="col col-mb-6 col-3">
				<div class="news-item-image">
					<a href="{url}" title="{name}">
						<xsl:choose>
							<xsl:when test="image_large !=''">
								<img src="{url}{image_large}" alt="Технические работы"/>
							</xsl:when>
							<xsl:otherwise>
								<img src="/images/noimage.png" alt="Технические работы"/>
							</xsl:otherwise>
						</xsl:choose>
					</a>
				</div> <!-- .news-item-image -->
			</div>
			<div class="col col-mb-6 col-9">
				<div class="news-item-date"><xsl:value-of select="date"/></div>
				<h3 class="h4 mt0"><a href="{url}" title="{name}"><xsl:value-of select="name"/></a></h3>
				<div class="clearfix"><xsl:value-of select="text"/><br/>
				</div>
			</div>


		</div>
	</xsl:template>
	
</xsl:stylesheet>