<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:hostcms="http://www.hostcms.ru/"
	exclude-result-prefixes="hostcms">
	
	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>
	
	<!-- СписокЭлементовИнфосистемы -->
	
	<xsl:template match="/">
		<xsl:apply-templates select="/informationsystem"/>
	</xsl:template>
	
	<xsl:variable name="n" select="number(3)"/>
	
	<xsl:template match="/informationsystem">
		<div class="vacancy-items col col-mb-12 col-8">
			<div class="clearfix">
				<xsl:apply-templates select="informationsystem_item"/>
			</div>
		</div>
		<div class="col col-mb-12 col-4">
			<div class="add-vacancy-form">
				<form id="js-add-vacancy-form">
				<div id="add-vacancy-form">			<div class="cn-modal">
					<div class="cn-modal-header">
						<b>Откликнуться на вакансию</b>				</div> <!-- .cn-modal-header -->
					<div class="cn-modal-content clearfix">

						<div class="clearfix"><div class="form-label">Вакансия* </div>
							<div class="form-control">
								<input type="text" name="vacancy" required="required" value="" class="input input-block-level"/>
							</div>
						</div>
						<div class="clearfix"><div class="form-label">ФИО* </div><div class="form-control"><input type="text" name="fio" required="required" value="" class="input input-block-level"/></div></div>
						<div class="clearfix"><div class="form-label">Возраст* </div><div class="form-control"><input type="text" name="old" required="required" value="" class="input input-block-level"/></div></div>
						<div class="clearfix"><div class="form-label">Телефон* </div><div class="form-control"><input type="text" name="phone" required="required" value="" class="input input-block-level"/></div></div>
						<div class="clearfix"><div class="form-label">Email </div><div class="form-control"><input type="text" name="email" value="" class="input input-block-level"/></div></div>
						<div class="clearfix">
						<div class="form-label">
						</div>
						<div class="form-control">
							Обязательно укажите предыдущие места работы
						</div>
					</div>
						<div class="clearfix mt10">
							<div class="form-label"> 
							</div>
							<div class="form-control">
								<button class="btn ladda-button" type="submit" name="submit_add_vacancy" data-style="expand-left"><span class="ladda-label">Отправить заявку</span></button>
							</div>
						</div>
					</div> <!-- .cn-modal-content -->

				</div> <!-- .cn-modal -->
					<input type="hidden" name="custom_form_code" value="add_vacancy"/>		</div> <!-- #add-vacancy-form -->
				</form>
			</div>
		</div>
	</xsl:template>

	<!-- Шаблон вывода информационного элемента -->
	<xsl:template match="informationsystem_item">
		<div class="vacancy-item mb10" id="bx_3218110189_5051">
			<h4 class="m0"><xsl:value-of select="name"/>
			<xsl:if test="property_value[tag_name='actual']/value = 1">
				<span class="vacancy-actual-label">Актуальная вакансия</span>
			</xsl:if>
			</h4>
		</div>

	</xsl:template>
	

</xsl:stylesheet>