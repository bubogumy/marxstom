<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet>
<xsl:stylesheet version="1.0"
				xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:hostcms="http://www.hostcms.ru/"
				exclude-result-prefixes="hostcms">

	<xsl:output xmlns="http://www.w3.org/TR/xhtml1/strict" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" encoding="utf-8" indent="yes" method="html" omit-xml-declaration="no" version="1.0" media-type="text/xml"/>

	<!-- СписокЭлементовИнфосистемы -->

	<xsl:template match="/">
		<xsl:apply-templates select="/informationsystem"/>
	</xsl:template>

	<xsl:variable name="n" select="number(3)"/>

	<xsl:template match="/informationsystem">

		<!-- Получаем ID родительской группы и записываем в переменную $group -->
		<xsl:variable name="group" select="group"/>

		<!-- Если в находимся корне - выводим название информационной системы -->

		<!-- Обработка выбранных тэгов -->
		<xsl:if test="count(tag)">
			<p class="h2">Метка — <strong><xsl:value-of select="tag/name" /></strong>.</p>
			<xsl:if test="tag/description != ''">
				<p><xsl:value-of select="tag/description" disable-output-escaping="yes" /></p>
			</xsl:if>
		</xsl:if>

		<!-- Отображение записи информационной системы -->
		<div class="col col-mb-12 col-12">
			<div class="h4">
					Хирургия
			</div>
			<xsl:apply-templates select="informationsystem_item" mode="surgery"/>
		</div>

		<div class="col col-mb-12 col-12">
			<div class="h4">
				Отропедия
			</div>
			<xsl:apply-templates select="informationsystem_item" mode="orthopedics"/>
		</div>

		<div class="col col-mb-12 col-12">
			<div class="h4">
				Терапия
			</div>
			<xsl:apply-templates select="informationsystem_item" mode="therapy"/>
		</div>

		<div class="col col-mb-12 col-12">
			<div class="h4">
				Зуботехническая лаборатория
			</div>
			<xsl:apply-templates select="informationsystem_item" mode="teeth_lab"/>
		</div>

		<div class="col col-mb-12 col-12">
			<div class="h4">
				Ортодонтия
			</div>
			<xsl:apply-templates select="informationsystem_item" mode="orthodontics"/>
		</div>

		<div class="col col-mb-12 col-12">
			<div class="h4">
				Младший медицинский персонал
			</div>
			<xsl:apply-templates select="informationsystem_item" mode="junior-med"/>
		</div>
	</xsl:template>

	<xsl:template match="informationsystem_item" mode="surgery">
		<xsl:if test="property_value[tag_name='surgery']/value = 1">
			<div class="personal-item equal" id="bx_3218110189_591">
					<div class="personal-item-image">
						<div class="personal-item-image-wrapper" data-target-self="{url}">
							<img src="{dir}{image_small}" alt="{name}" title="{name}"/>
						</div>
						<a
								class="personal-item-feedback"
								title="{name}"
								href="{url}"
						>
							<xsl:value-of select="comments_count"/>								</a>
					</div> <!-- .personal-item-image -->
					<div class="personal-item-text">
						<div class="h4">
							<a href="{url}"><xsl:value-of select="name" /></a>
						</div>
						<div class="personal-item-text-description"><xsl:value-of select="description" disable-output-escaping="yes" /><div>
							<xsl:value-of select="text" disable-output-escaping="yes" /></div>
						</div>

					</div> <!-- .personal-item-text -->
				</div> <!-- .personal-item equal -->
			</xsl:if>
	</xsl:template>

	<xsl:template match="informationsystem_item" mode="orthopedics">
		<xsl:if test="property_value[tag_name='orthopedics']/value = 1">
			<div class="personal-item equal" id="bx_3218110189_591">
				<div class="personal-item-image">
					<div class="personal-item-image-wrapper" data-target-self="{url}">
						<img src="{dir}{image_small}" alt="{name}" title="{name}"/>
					</div>
					<a
							class="personal-item-feedback"
							title="{name}"
							href="{url}"
					>
						<xsl:value-of select="comments_count"/>								</a>
				</div> <!-- .personal-item-image -->
				<div class="personal-item-text">
					<div class="h4">
						<a href="{url}"><xsl:value-of select="name" /></a>
					</div>
					<div class="personal-item-text-description"><xsl:value-of select="description" disable-output-escaping="yes" /><div>
						<xsl:value-of select="text" disable-output-escaping="yes" /></div>
					</div>

				</div> <!-- .personal-item-text -->
			</div> <!-- .personal-item equal -->
		</xsl:if>
	</xsl:template>

	<xsl:template match="informationsystem_item" mode="therapy">
		<xsl:if test="property_value[tag_name='therapy']/value = 1">
			<div class="personal-item equal" id="bx_3218110189_591">
				<div class="personal-item-image">
					<div class="personal-item-image-wrapper" data-target-self="{url}">
						<img src="{dir}{image_small}" alt="{name}" title="{name}"/>
					</div>
					<a
							class="personal-item-feedback"
							title="{name}"
							href="{url}"
					>
						<xsl:value-of select="comments_count"/>								</a>
				</div> <!-- .personal-item-image -->
				<div class="personal-item-text">
					<div class="h4">
						<a href="{url}"><xsl:value-of select="name" /></a>
					</div>
					<div class="personal-item-text-description"><xsl:value-of select="description" disable-output-escaping="yes" /><div>
						<xsl:value-of select="text" disable-output-escaping="yes" /></div>
					</div>

				</div> <!-- .personal-item-text -->
			</div> <!-- .personal-item equal -->
		</xsl:if>
	</xsl:template>

	<xsl:template match="informationsystem_item" mode="teeth_lab">
		<xsl:if test="property_value[tag_name='teeth_lab']/value = 1">
			<div class="personal-item equal" id="bx_3218110189_591">
				<div class="personal-item-image">
					<div class="personal-item-image-wrapper" data-target-self="{url}">
						<img src="{dir}{image_small}" alt="{name}" title="{name}"/>
					</div>
					<a
							class="personal-item-feedback"
							title="{name}"
							href="{url}"
					>
						<xsl:value-of select="comments_count"/>								</a>
				</div> <!-- .personal-item-image -->
				<div class="personal-item-text">
					<div class="h4">
						<a href="{url}"><xsl:value-of select="name" /></a>
					</div>
					<div class="personal-item-text-description"><xsl:value-of select="description" disable-output-escaping="yes" /><div>
						<xsl:value-of select="text" disable-output-escaping="yes" /></div>
					</div>

				</div> <!-- .personal-item-text -->
			</div> <!-- .personal-item equal -->
		</xsl:if>
	</xsl:template>

	<xsl:template match="informationsystem_item" mode="orthodontics">
		<xsl:if test="property_value[tag_name='orthodontics']/value = 1">
			<div class="personal-item equal" id="bx_3218110189_591">
				<div class="personal-item-image">
					<div class="personal-item-image-wrapper" data-target-self="{url}">
						<img src="{dir}{image_small}" alt="{name}" title="{name}"/>
					</div>
					<a
							class="personal-item-feedback"
							title="{name}"
							href="{url}"
					>
						<xsl:value-of select="comments_count"/>								</a>
				</div> <!-- .personal-item-image -->
				<div class="personal-item-text">
					<div class="h4">
						<a href="{url}"><xsl:value-of select="name" /></a>
					</div>
					<div class="personal-item-text-description"><xsl:value-of select="description" disable-output-escaping="yes" /><div>
						<xsl:value-of select="text" disable-output-escaping="yes" /></div>
					</div>

				</div> <!-- .personal-item-text -->
			</div> <!-- .personal-item equal -->
		</xsl:if>
	</xsl:template>

	<xsl:template match="informationsystem_item" mode="junior-med">
		<xsl:if test="property_value[tag_name='junior-med']/value = 1">
			<div class="personal-item equal" id="bx_3218110189_591">
				<div class="personal-item-image">
					<div class="personal-item-image-wrapper" data-target-self="{url}">
						<img src="{dir}{image_small}" alt="{name}" title="{name}"/>
					</div>
					<a
							class="personal-item-feedback"
							title="{name}"
							href="{url}"
					>
						<xsl:value-of select="comments_count"/>								</a>
				</div> <!-- .personal-item-image -->
				<div class="personal-item-text">
					<div class="h4">
						<a href="{url}"><xsl:value-of select="name" /></a>
					</div>
					<div class="personal-item-text-description"><xsl:value-of select="description" disable-output-escaping="yes" /><div>
						<xsl:value-of select="text" disable-output-escaping="yes" /></div>
					</div>

				</div> <!-- .personal-item-text -->
			</div> <!-- .personal-item equal -->
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>