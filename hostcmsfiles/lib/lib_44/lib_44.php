
<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && key_exists('rate_filter', $_POST)) {

    if (Core_Array::getPost('rate_filter') == 5) {
        if (Core::moduleIsActive('informationsystem'))
        {
            $Informationsystem_Controller_Show = new Informationsystem_Controller_Show(
                Core_Entity::factory('Informationsystem', 6)
            );
            $Informationsystem_Controller_Show
                ->xsl(
                    Core_Entity::factory('Xsl')->getByName('ОтзывыПоложительные')
                )
                ->groupsMode('none')
                ->itemsForbiddenTags(array('text'))
                ->itemsProperties(true)
                ->group(FALSE)
                ->comments(TRUE)
                ->limit(999)
                ->show();
        }
    } elseif(Core_Array::getPost('rate_filter') == 3) {
        if (Core::moduleIsActive('informationsystem'))
        {
            $Informationsystem_Controller_Show = new Informationsystem_Controller_Show(
                Core_Entity::factory('Informationsystem', 6)
            );
            $Informationsystem_Controller_Show
                ->xsl(
                    Core_Entity::factory('Xsl')->getByName('ОтзывыНейтральные')
                )
                ->groupsMode('none')
                ->itemsForbiddenTags(array('text'))
                ->itemsProperties(true)
                ->group(FALSE)
                ->comments(TRUE)
                ->limit(999)
                ->show();
        }
    } elseif(Core_Array::getPost('rate_filter') == 1) {
        if (Core::moduleIsActive('informationsystem'))
        {
            $Informationsystem_Controller_Show = new Informationsystem_Controller_Show(
                Core_Entity::factory('Informationsystem', 6)
            );
            $Informationsystem_Controller_Show
                ->xsl(
                    Core_Entity::factory('Xsl')->getByName('ОтзывыОтрицательные')
                )
                ->groupsMode('none')
                ->itemsForbiddenTags(array('text'))
                ->itemsProperties(true)
                ->group(FALSE)
                ->comments(TRUE)
                ->limit(999)
                ->show();
        }
    } else {
        // Селектор врачей
        if (Core::moduleIsActive('informationsystem'))
        {
            $Informationsystem_Controller_Show = new Informationsystem_Controller_Show(
                Core_Entity::factory('Informationsystem', 6)
            );
            $Informationsystem_Controller_Show
                ->xsl(
                    Core_Entity::factory('Xsl')->getByName('Отзывы')
                )
                ->groupsMode('none')
                ->itemsForbiddenTags(array('text'))
                ->itemsProperties(true)
                ->group(FALSE)
                ->comments(TRUE)
                ->limit(999)
                ->show();
        }
    }
} else {

    // Селектор врачей
    if (Core::moduleIsActive('informationsystem'))
    {
        $Informationsystem_Controller_Show = new Informationsystem_Controller_Show(
            Core_Entity::factory('Informationsystem', 6)
        );
        $Informationsystem_Controller_Show
        ->xsl(
            Core_Entity::factory('Xsl')->getByName('Отзывы')
            )
            ->groupsMode('none')
            ->itemsForbiddenTags(array('text'))
            ->itemsProperties(true)
            ->group(FALSE)
            ->comments(TRUE)
            ->limit(999)
            ->show();
    }
}

                                ?>